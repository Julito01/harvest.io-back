# Harvest.io

```
API para guardar, obtener y modificar datos de usuarios, huertas, parcelas, sensores y mediciones
que se utilizaran para administrar los cultivos de un productor.
Apuntado a pequeños productores.
```

## Levantar proyecto localmente

Para levantar el proyecto debe tener instalado **_Docker_** en su computadora.
`https://www.docker.com/products/docker-desktop/`

## Clonar repositorio

Crear una carpeta para alojar el código fuente de la aplicación.
Luego, ingresar a esa carpeta usando `cd {nombre_carpeta}`

Dentro de la carpeta utilizar el siguiente comando

```
git clone https://gitlab.com/Julito01/harvest.io-back.git
```

## Acceda al directorio clonado en una terminal

```
cd harvest.io-back
```

## Cree un archivo con extensión .sql
Dentro de la ruta `harvest.io-back/src/main/resources/` cree un archivo llamado `init.sql`.

Dentro de él pegue este código
```roomsql
CREATE USER 'harvest'@'%' IDENTIFIED BY 'harvestio2023';
GRANT ALL PRIVILEGES ON harvest_io.* TO 'harvest'@'%';
FLUSH PRIVILEGES;
```

## Cree el archivo de environments
Dentro de la ruta `harvest.io-back/src/main/resources/` cree un archivo llamado `.env` y agregue las líneas
que se encuentran en el archivo `.env.template`

## Ingresar comandos _Docker_

```
docker-compose up
```

## Ingresar a la aplicación

Abra una navegador web e ingrese a `localhost:80/actuator/health`
