package com.harvestio.harvest.io.repository;

import com.harvestio.harvest.io.model.Crop;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CropRepository extends JpaRepository<Crop, Integer> {
}
