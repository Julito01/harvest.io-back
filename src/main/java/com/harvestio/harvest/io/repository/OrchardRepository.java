package com.harvestio.harvest.io.repository;

import com.harvestio.harvest.io.model.Orchard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrchardRepository extends JpaRepository<Orchard, Integer> {
}
