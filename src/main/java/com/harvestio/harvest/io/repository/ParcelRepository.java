package com.harvestio.harvest.io.repository;

import com.harvestio.harvest.io.model.Parcel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ParcelRepository extends JpaRepository <Parcel, Integer> {
}
