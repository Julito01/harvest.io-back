package com.harvestio.harvest.io.repository;

import com.harvestio.harvest.io.dto.MeasDTO;
import com.harvestio.harvest.io.model.Meas;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
@Repository
public interface MeasRepository extends JpaRepository<Meas, Integer> {
    @Query("SELECT m FROM Meas m WHERE m.sensor.id = :sensor_id AND FUNCTION('DATE', m.createdAt) >= :created_at")
    public List<Meas> last30DaysReads(@Param("sensor_id") Integer id, @Param("created_at") Date date);

    @Query("SELECT FUNCTION('DATE', m.createdAt) as createdAt, AVG(m.read) " +
            "FROM Meas m JOIN m.sensor s " +
            "WHERE s.parcel.id = :parcelId AND FUNCTION('DATE', m.createdAt) >= :startDate " +
            "GROUP BY FUNCTION('DATE', m.createdAt) ORDER BY createdAt DESC")
    public List<Object> getAverageSoilLast30Days(@Param("parcelId") Integer id, @Param("startDate") Date date);
}