package com.harvestio.harvest.io.repository;

import com.harvestio.harvest.io.model.Sensor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;

@Repository
public interface SensorRepository extends JpaRepository <Sensor, Integer> {
}
