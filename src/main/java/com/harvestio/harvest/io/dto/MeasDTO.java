package com.harvestio.harvest.io.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.harvestio.harvest.io.model.BaseModel;
import com.harvestio.harvest.io.model.Sensor;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.sql.Date;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
public class MeasDTO extends BaseModel {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Integer id;
    private Double read;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Integer sensorId;
}
