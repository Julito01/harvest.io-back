package com.harvestio.harvest.io.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.harvestio.harvest.io.model.Crop;
import com.harvestio.harvest.io.model.Orchard;
import com.harvestio.harvest.io.model.Sensor;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ParcelDTO {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Integer id;
    private String soilType;
    private Double size;
    private Double ph;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Integer orchardId;

    @JsonProperty(access = JsonProperty.Access.READ_WRITE)
    private Integer cropId;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private List<Integer> sensorIds;
}
