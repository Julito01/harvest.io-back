package com.harvestio.harvest.io.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.harvestio.harvest.io.model.Parcel;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;
@NoArgsConstructor
@AllArgsConstructor
@Data
public class CropDTO {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Integer id;
    private String name;
    private String idealSoil;
    private Double idealPh;
    private Double idealMoisture;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private List<Integer> parcelIds;
}
