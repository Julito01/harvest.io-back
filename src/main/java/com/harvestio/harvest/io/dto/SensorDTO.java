package com.harvestio.harvest.io.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.harvestio.harvest.io.model.Meas;
import com.harvestio.harvest.io.model.Parcel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class SensorDTO {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Integer id;
    private Double soilMoisture;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private List<Integer> measListId;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Integer parcelId;
}