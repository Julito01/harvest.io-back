package com.harvestio.harvest.io.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class OrchardDTO {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Integer id;
    private String province;
    private String city;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private List<Integer> parcelIds;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Integer userId;
}