package com.harvestio.harvest.io.model;

public enum Role {
    USER,
    ADMIN
}
