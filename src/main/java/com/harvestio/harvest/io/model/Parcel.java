package com.harvestio.harvest.io.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class Parcel extends BaseModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "soil_type")
    private String soilType;
    private Double size;
    private Double ph;

    @ManyToOne
    @JoinColumn (name = "orchard_id")
    @JsonIgnore
    private Orchard orchard;

    @ManyToOne
    @JoinColumn (name = "crop_id")
    @JsonIgnore
    private Crop crop;

    @OneToMany (mappedBy = "parcel")
    private List<Sensor> sensors;
}
