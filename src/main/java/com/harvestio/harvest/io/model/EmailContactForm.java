package com.harvestio.harvest.io.model;

import lombok.Data;

@Data
public class EmailContactForm {
    private String name;
    private String lastName;
    private String dni;
    private String phone;
    private String email;
}
