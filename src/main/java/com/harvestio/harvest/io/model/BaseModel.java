package com.harvestio.harvest.io.model;

import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import lombok.Data;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

@Data
@MappedSuperclass
public class BaseModel {
    @Column(name = "created_at", nullable = false)
    private Date createdAt;
    @Column(name = "updated_at", nullable = false)
    private Date updatedAt;
    private boolean active;

    public void setDefaultAttributes() {
        this.setCreatedAt(this.getDate());
        this.setUpdatedAt(this.getDate());
        this.setActive(true);
    }

    public Date getDate() {
        ZonedDateTime nowBuenosAires = ZonedDateTime.now(ZoneId.of("America/Argentina/Buenos_Aires"));
        return Date.from(nowBuenosAires.toInstant());
    }
}
