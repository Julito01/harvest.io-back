package com.harvestio.harvest.io.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Crop extends BaseModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    @Column(name = "ideal_soil")
    private String idealSoil;

    @Column(name = "ideal_ph")
    private Double idealPh;

    @Column(name = "ideal_moisture")
    private Double idealMoisture;

    @OneToMany(mappedBy = "crop")
    private List<Parcel> parcels;
}
