package com.harvestio.harvest.io.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class Sensor extends BaseModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "soil_moisture")
    private Double soilMoisture;

    @OneToOne
    @JoinColumn (name = "parcel_id")
    @JsonIgnore
    private Parcel parcel;

    @OneToMany(mappedBy = "sensor")
    private List<Meas> measurements;
}
