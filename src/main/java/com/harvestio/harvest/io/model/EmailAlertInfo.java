package com.harvestio.harvest.io.model;

import lombok.Data;

@Data
public class EmailAlertInfo {
    private String parcelId;
    private String subject;
    private String type;
    private String warningMessage;
    private String userEmail;
}
