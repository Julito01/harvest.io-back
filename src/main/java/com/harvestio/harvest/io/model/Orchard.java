package com.harvestio.harvest.io.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class Orchard extends BaseModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String province;
    private String city;

    @OneToMany(mappedBy = "orchard")
    private List<Parcel> parcels;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;
}
