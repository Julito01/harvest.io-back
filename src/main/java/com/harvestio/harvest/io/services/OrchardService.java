package com.harvestio.harvest.io.services;

import com.harvestio.harvest.io.exception.ApiRequestException;
import com.harvestio.harvest.io.repository.OrchardRepository;
import com.harvestio.harvest.io.dto.OrchardDTO;
import com.harvestio.harvest.io.dto.ParcelDTO;
import com.harvestio.harvest.io.model.*;
import com.harvestio.harvest.io.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.http.HttpStatus.*;

@Service
public class OrchardService {

    private final OrchardRepository orchardRepository;
    private final UserRepository userRepository;
    private final ModelMapper modelMapper;
    private final String errorMessage = "La huerta con id [%s] no se encontró.";

    public OrchardService(OrchardRepository orchardRepository,
                          UserRepository userRepository,
                          ModelMapper modelMapper) {
        this.orchardRepository = orchardRepository;
        this.userRepository = userRepository;
        this.modelMapper = modelMapper;
    }

    public ResponseEntity<List<OrchardDTO>> getAll() {
        List<Orchard> orchards = this.orchardRepository.findAll();
        List<OrchardDTO> orchardDTOS = new ArrayList<>();

        for (Orchard orchard : orchards) {
            if (orchard.isActive())
                orchardDTOS.add(this.modelMapper.map(orchard, OrchardDTO.class));
        }

        return ResponseEntity.ok(orchardDTOS);
    }

    public ResponseEntity<OrchardDTO> get(Integer orchardId) {
        Orchard orchard = this.orchardRepository.findById(orchardId)
                .orElseThrow(
                        () -> new ApiRequestException(
                                String.format(this.errorMessage, orchardId),
                                NOT_FOUND
                        )
                );

        return ResponseEntity.ok(this.modelMapper.map(orchard, OrchardDTO.class));
    }

    public ResponseEntity<OrchardDTO> add(OrchardDTO orchardDTO) {
        int userId = orchardDTO.getUserId();
        User user = this.userRepository.findById(userId)
                .orElseThrow(
                        () -> new ApiRequestException(
                                String.format("El usuario con id [%s] no se encontró", userId),
                                NOT_FOUND
                        )
                );

        Orchard orchard = this.modelMapper.map(orchardDTO, Orchard.class);

        orchard.setUser(user);
        orchard.setDefaultAttributes();

        Orchard createdOrchard = this.orchardRepository.save(orchard);
        OrchardDTO createdOrchardDTO = this.modelMapper.map(createdOrchard, OrchardDTO.class);
        createdOrchardDTO.setParcelIds(new ArrayList<>());

        return ResponseEntity.status(CREATED).body(createdOrchardDTO);
    }

    public ResponseEntity<OrchardDTO> delete(Integer orchardId) {
        Orchard orchardFound = this.orchardRepository.findById(orchardId)
                .orElseThrow(
                        () -> new ApiRequestException(
                                String.format(this.errorMessage, orchardId),
                                NOT_FOUND
                        )
                );

        orchardFound.setUpdatedAt(orchardFound.getDate());
        orchardFound.setActive(false);
        Orchard deletedOrchard = this.orchardRepository.save(orchardFound);

        return ResponseEntity.ok(this.modelMapper.map(deletedOrchard, OrchardDTO.class));
    }

    public ResponseEntity<OrchardDTO> update(Integer orchardId, OrchardDTO orchardDTO) {
        Orchard orchard = this.orchardRepository.findById(orchardId)
                .orElseThrow(
                        () -> new ApiRequestException(
                                String.format(this.errorMessage, orchardId),
                                NOT_FOUND
                        )
                );

        orchard.setProvince(orchardDTO.getProvince());
        orchard.setCity(orchardDTO.getCity());
        orchard.setUpdatedAt(orchard.getDate());
        Orchard updatedOrchard = this.orchardRepository.save(orchard);

        return ResponseEntity.ok(this.modelMapper.map(updatedOrchard, OrchardDTO.class));
    }

    public ResponseEntity<List<ParcelDTO>> getParcels(Integer orchardId) {
        Orchard orchard = this.orchardRepository.findById(orchardId)
                .orElseThrow(
                        () -> new ApiRequestException(
                                String.format(this.errorMessage, orchardId),
                                NOT_FOUND
                        )
                );

        List<ParcelDTO> parcelDTOS = new ArrayList<>();

        for (Parcel parcel : orchard.getParcels()) {
            if (!parcel.isActive()) continue;
            ParcelDTO parcelDTO = this.modelMapper.map(parcel, ParcelDTO.class);
            parcelDTO.setSensorIds(
                    parcel.getSensors()
                            .stream()
                            .map(Sensor::getId)
                            .toList()
            );
            parcelDTOS.add(parcelDTO);
        }

        return ResponseEntity.ok(parcelDTOS);
    }
}