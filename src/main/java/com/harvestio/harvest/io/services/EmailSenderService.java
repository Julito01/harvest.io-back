package com.harvestio.harvest.io.services;

import com.harvestio.harvest.io.auth.RegisterRequest;
import com.harvestio.harvest.io.model.EmailAlertInfo;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import java.io.StringWriter;
import java.util.HashMap;

@Service
public class EmailSenderService {
    private final JavaMailSender mailSender;
    private final VelocityEngine velocityEngine;

    public EmailSenderService(JavaMailSender mailSender, VelocityEngine velocityEngine) {
        this.mailSender = mailSender;
        this.velocityEngine = velocityEngine;
    }

    public String prepareRegistrationEmailText(RegisterRequest request) {
        VelocityContext context = new VelocityContext();
        context.put("firstName", request.getFirstName());
        context.put("lastName", request.getLastName());
        context.put("docId", request.getDocId());
        context.put("email", request.getEmail());
        context.put("cellphone", request.getCellphone());
        StringWriter stringWriter = new StringWriter();
        velocityEngine.mergeTemplate("registration.vm", "UTF-8", context, stringWriter);
        return stringWriter.toString();
    }

    public String prepareAlertEmailText(EmailAlertInfo request){
        VelocityContext context = new VelocityContext();
        context.put("parcel", request.getParcelId());
        context.put("type", request.getType());
        context.put("message", request.getWarningMessage());
        StringWriter stringWriter = new StringWriter();
        velocityEngine.mergeTemplate("alert.vm", "UTF-8", context, stringWriter);
        return stringWriter.toString();
    }

    public  ResponseEntity<HashMap<String, Object>> sendAlert (EmailAlertInfo request) throws MessagingException{
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, "utf-8");

        mimeMessage.setContent(prepareAlertEmailText(request), "text/html");
        helper.setFrom("harvestio2023@gmail.com");
        helper.setTo(request.getUserEmail());
        helper.setSubject(request.getSubject());

        this.mailSender.send(mimeMessage);

        HashMap<String, Object> response = new HashMap<>();
        response.put("result", true);
        response.put("message", "Email a " + request.getUserEmail()+ " enviado éxitosamente.");

        return ResponseEntity.ok(response);

    }

    public ResponseEntity<HashMap<String, Object>> sendMail(RegisterRequest request) throws MessagingException {
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, "utf-8");

        mimeMessage.setContent(prepareRegistrationEmailText(request), "text/html");
        helper.setFrom("harvestio2023@gmail.com");
        helper.setTo("harvestio2023@gmail.com");
        helper.setSubject("Asesoramiento - " + request.getFirstName() + " " + request.getLastName());

        this.mailSender.send(mimeMessage);

        HashMap<String, Object> response = new HashMap<>();
        response.put("result", true);
        response.put("message", "Email a " + request.getEmail() + " enviado con éxito.");

        return ResponseEntity.ok(response);
    }
}
