package com.harvestio.harvest.io.services;

import java.util.*;

import com.harvestio.harvest.io.dto.MeasDTO;
import com.harvestio.harvest.io.exception.ApiRequestException;
import com.harvestio.harvest.io.model.Meas;
import com.harvestio.harvest.io.model.Sensor;
import com.harvestio.harvest.io.repository.MeasRepository;
import com.harvestio.harvest.io.repository.SensorRepository;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import static org.springframework.http.HttpStatus.*;

@Service
public class MeasService {

    private final MeasRepository measRepository;
    private final SensorRepository sensorRepository;
    private final ModelMapper modelMapper;
    private final String errorMessage = "La medición con id [%s] no se encontró.";

    public MeasService(MeasRepository measRepository,
                       SensorRepository sensorRepository,
                       ModelMapper modelMapper) {
        this.measRepository = measRepository;
        this.sensorRepository = sensorRepository;
        this.modelMapper = modelMapper;
    }

    public ResponseEntity<List<MeasDTO>> getAll() {
        List<Meas> measurements = this.measRepository.findAll();
        List<MeasDTO> measDTOS = new ArrayList<>();

        for (Meas measurement : measurements) {
            measDTOS.add(this.modelMapper.map(measurement, MeasDTO.class));
        }

        return ResponseEntity.ok(measDTOS);
    }

    public ResponseEntity<MeasDTO> add(MeasDTO measurementDTO) {
        int sensorId = measurementDTO.getSensorId();

        Sensor sensor = this.sensorRepository.findById(sensorId)
                .orElseThrow(() -> new ApiRequestException(
                                String.format("El sensor con id [%s] no se encontró", sensorId),
                                NOT_FOUND
                        )
                );

        Meas measurement = this.modelMapper.map(measurementDTO, Meas.class);

        measurement.setSensor(sensor);
        sensor.setSoilMoisture(measurementDTO.getRead());
        measurement.setDefaultAttributes();

        this.measRepository.save(measurement);

        return ResponseEntity.status(CREATED).body(measurementDTO);
    }

    public ResponseEntity<MeasDTO> update(Integer measId, MeasDTO measDTO) {
        Meas meas = this.measRepository.findById(measId)
                .orElseThrow(() -> new ApiRequestException(
                                String.format(this.errorMessage, measId),
                                NOT_FOUND
                        )
                );

        meas.setUpdatedAt(meas.getDate());
        meas.setRead(measDTO.getRead());

        Meas updatedMeas = this.measRepository.save(meas);

        return ResponseEntity.ok(this.modelMapper.map(updatedMeas, MeasDTO.class));
    }

    public ResponseEntity<Map<String, Object>> delete(Integer measurementId) {
        this.measRepository.deleteById(measurementId);

        Map<String, Object> response = new HashMap<>();
        response.put("result", true);
        response.put("message", "Medición borrada");

        return ResponseEntity.ok(response);
    }
}
