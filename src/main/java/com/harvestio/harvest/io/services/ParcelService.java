package com.harvestio.harvest.io.services;

import com.harvestio.harvest.io.dto.MeasDTO;
import com.harvestio.harvest.io.dto.SensorDTO;
import com.harvestio.harvest.io.exception.ApiRequestException;
import com.harvestio.harvest.io.repository.CropRepository;
import com.harvestio.harvest.io.repository.OrchardRepository;
import com.harvestio.harvest.io.repository.ParcelRepository;
import com.harvestio.harvest.io.dto.ParcelDTO;
import com.harvestio.harvest.io.model.*;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.http.HttpStatus.*;

@Service
public class ParcelService {

    private final ParcelRepository parcelRepository;
    private final OrchardRepository orchardRepository;
    private final CropRepository cropRepository;
    private final SensorService sensorService;

    private final ModelMapper modelMapper;

    private final String errorMessage = "Parcela con id [%s] no encontrada.";

    public ParcelService(ParcelRepository parcelRepository,
                         SensorService sensorService,
                         OrchardRepository orchardRepository,
                         CropRepository cropRepository,
                         ModelMapper modelMapper) {
        this.parcelRepository = parcelRepository;
        this.sensorService = sensorService;
        this.orchardRepository = orchardRepository;
        this.cropRepository = cropRepository;
        this.modelMapper = modelMapper;
    }

    public ResponseEntity<ParcelDTO> get(Integer parcelId) {
        Parcel parcel = this.parcelRepository.findById(parcelId)
                .orElseThrow(
                        () -> new ApiRequestException(
                                String.format(this.errorMessage, parcelId),
                                NOT_FOUND
                        )
                );
        ParcelDTO parcelMapped = this.modelMapper.map(parcel, ParcelDTO.class);
        parcelMapped.setCropId(parcel.getCrop().getId());
        return ResponseEntity.ok(parcelMapped);
    }

    public List<ParcelDTO> getAll() {

        List<Parcel> parcels = this.parcelRepository.findAll();
        List<ParcelDTO> parcelDTOS = new ArrayList<>();

        for (Parcel parcel : parcels) {
            if (!parcel.isActive()) continue;
            ParcelDTO parcelDTO = this.modelMapper.map(parcel, ParcelDTO.class);
            parcelDTO.setSensorIds(
                    parcel.getSensors()
                            .stream()
                            .map(Sensor::getId)
                            .toList()
            );
            parcelDTOS.add(parcelDTO);
        }

        return parcelDTOS;
    }

    public ResponseEntity<ParcelDTO> add(ParcelDTO parcelDTO) {
        Integer orchardId = parcelDTO.getOrchardId();
        Integer cropId = parcelDTO.getCropId();

        if (orchardId == null)
            throw new ApiRequestException("El id de la huerta no puede ser nulo", BAD_REQUEST);

        if (cropId == null)
            throw new ApiRequestException("El id del cultivo no puede ser nulo", BAD_REQUEST);

        Orchard orchard = this.orchardRepository.findById(orchardId)
                .orElseThrow(
                        () -> new ApiRequestException(
                                String.format("La huerta con id [%s] no se encontró", orchardId),
                                NOT_FOUND
                        )
                );

        Crop crop = this.cropRepository.findById(cropId)
                .orElseThrow(
                        () -> new ApiRequestException(
                                String.format("El cultivo con id [%s] no se encontró", cropId),
                                NOT_FOUND
                        )
                );

        Parcel parcel = this.modelMapper.map(parcelDTO, Parcel.class);

        parcel.setOrchard(orchard);
        parcel.setCrop(crop);
        parcel.setDefaultAttributes();

        Parcel createdParcel = this.parcelRepository.save(parcel);

        return ResponseEntity.status(CREATED).body(this.modelMapper.map(createdParcel, ParcelDTO.class));
    }

    public ResponseEntity<ParcelDTO> update(Integer parcelId, ParcelDTO parcelDTO) {
        Parcel parcel = this.parcelRepository.findById(parcelId)
                .orElseThrow(
                        () -> new ApiRequestException(
                                String.format(this.errorMessage, parcelId),
                                NOT_FOUND
                        )
                );

        Crop crop = this.cropRepository.findById(parcelDTO.getCropId())
                .orElseThrow(
                    () -> new ApiRequestException(
                        String.format(this.errorMessage, parcelId),
                        NOT_FOUND
                )
        );
        parcel.setPh(parcelDTO.getPh());
        parcel.setSize(parcelDTO.getSize());
        parcel.setSoilType(parcelDTO.getSoilType());
        parcel.setUpdatedAt(parcel.getDate());
        parcel.setCrop(crop);


        Parcel updatedParcel = this.parcelRepository.save(parcel);

        return ResponseEntity.ok(this.modelMapper.map(updatedParcel, ParcelDTO.class));
    }

    public ResponseEntity<ParcelDTO> delete(Integer parcelId) {
        Parcel parcel = this.parcelRepository.findById(parcelId)
                .orElseThrow(
                        () -> new ApiRequestException(
                                String.format(this.errorMessage, parcelId),
                                NOT_FOUND
                        )
                );

        parcel.setUpdatedAt(parcel.getDate());
        parcel.setActive(false);

        Parcel updatedParcel = this.parcelRepository.save(parcel);

        return ResponseEntity.ok(this.modelMapper.map(updatedParcel, ParcelDTO.class));
    }

    public ResponseEntity<List<SensorDTO>> getSensors(Integer parcelId) {
        Parcel parcel = this.parcelRepository.findById(parcelId)
                .orElseThrow(
                        () -> new ApiRequestException(
                                String.format(this.errorMessage, parcelId),
                                NOT_FOUND
                        )
                );

        List<SensorDTO> sensorDTOS = new ArrayList<>();

        for (Sensor sensor : parcel.getSensors()) {
            if (!sensor.isActive()) continue;
            SensorDTO sensorDTO = this.modelMapper.map(sensor, SensorDTO.class);
            sensorDTO.setMeasListId(sensor.getMeasurements()
                    .stream()
                    .map(Meas::getId)
                    .toList()
            );
            sensorDTOS.add(sensorDTO);
        }

        return ResponseEntity.ok(sensorDTOS);
    }

    public ResponseEntity<List<Object>> getLast30Days(Integer parcelId) {

        List<Object> measDTOS = new ArrayList<>(this.sensorService.getLast30days(parcelId));

        return ResponseEntity.ok(measDTOS);
    }

    public ResponseEntity<Double> getSoil (Integer parcelId){
        Parcel parcel = this.parcelRepository.findById(parcelId).orElseThrow(
                () -> new ApiRequestException(
                        String.format(this.errorMessage, parcelId),
                        NOT_FOUND
                )
        );
        double soil = 0;
        double totalSoil;
        for (Sensor sensor : parcel.getSensors()){
            soil += sensor.getSoilMoisture();
        }

        totalSoil = soil / parcel.getSensors().size();

        return ResponseEntity.ok(totalSoil);
    }
}
