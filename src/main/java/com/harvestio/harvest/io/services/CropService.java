package com.harvestio.harvest.io.services;

import com.harvestio.harvest.io.dto.CropDTO;
import com.harvestio.harvest.io.dto.ParcelDTO;
import com.harvestio.harvest.io.exception.ApiRequestException;
import com.harvestio.harvest.io.model.Crop;
import com.harvestio.harvest.io.model.Parcel;
import com.harvestio.harvest.io.repository.CropRepository;
import com.harvestio.harvest.io.repository.ParcelRepository;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.http.HttpStatus.*;

@Service
public class CropService {

    private final CropRepository cropRepository;
    private final ParcelRepository parcelRepository;
    private final ModelMapper modelMapper;
    private final String errorMessage = "El cultivo con id [%s] no se encontró.";

    public CropService(CropRepository cropRepository,
                       ParcelRepository parcelRepository,
                       ModelMapper modelMapper) {
        this.cropRepository = cropRepository;
        this.parcelRepository = parcelRepository;
        this.modelMapper = modelMapper;
    }

    public ResponseEntity<List<CropDTO>> getAll() {
        List<CropDTO> cropDTOS = new ArrayList<>();

        for (Crop crop : this.cropRepository.findAll()) {
            cropDTOS.add(this.modelMapper.map(crop, CropDTO.class));
        }

        return ResponseEntity.ok(cropDTOS);
    }

    public ResponseEntity<CropDTO> getCrop(Integer cropId) {
        Crop crop = this.cropRepository.findById(cropId)
                .orElseThrow(
                        () -> new ApiRequestException(
                                String.format(this.errorMessage, cropId),
                                NOT_FOUND
                        )
                );

        return ResponseEntity.ok(this.modelMapper.map(crop, CropDTO.class));
    }

    public ResponseEntity<CropDTO> addCrop(CropDTO cropDTO) {
        List<ParcelDTO> parcelDTOS = new ArrayList<>();

        for (int parcelId : cropDTO.getParcelIds()) {
            Parcel parcel = this.parcelRepository.findById(parcelId)
                    .orElseThrow(
                            () -> new ApiRequestException(
                                    String.format("La parcela con id [%s] no se encontró", parcelId),
                                    NOT_FOUND)
                    );
            parcelDTOS.add(this.modelMapper.map(parcel, ParcelDTO.class));
        }

        Crop crop = this.modelMapper.map(cropDTO, Crop.class);

        crop.setDefaultAttributes();

        Crop createdCrop = this.cropRepository.save(crop);

        return ResponseEntity.status(CREATED).body(this.modelMapper.map(createdCrop, CropDTO.class));
    }
}
