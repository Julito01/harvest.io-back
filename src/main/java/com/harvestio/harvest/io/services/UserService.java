package com.harvestio.harvest.io.services;

import com.harvestio.harvest.io.dto.UserDTO;
import com.harvestio.harvest.io.exception.ApiRequestException;
import com.harvestio.harvest.io.repository.OrchardRepository;
import com.harvestio.harvest.io.repository.UserRepository;
import com.harvestio.harvest.io.dto.OrchardDTO;
import com.harvestio.harvest.io.model.*;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.http.HttpStatus.*;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final ModelMapper modelMapper;
    private final String errorMessage = "El usuario con id [%s] no se encontró.";

    public UserService(UserRepository userRepository,
                       ModelMapper modelMapper) {
        this.userRepository = userRepository;
        this.modelMapper = modelMapper;
    }

    public ResponseEntity<UserDTO> get(Integer userId) {
        User user = this.userRepository.findById(userId)
                .orElseThrow(
                        () -> new ApiRequestException(
                                String.format(this.errorMessage, userId),
                                NOT_FOUND
                        )
                );
        return ResponseEntity.ok(this.modelMapper.map(user, UserDTO.class));
    }

    public ResponseEntity<List<UserDTO>> getAll() {
        List<User> users = this.userRepository.findAll();
        List<UserDTO> userDTOS = new ArrayList<>();

        for (User user : users) {
            if (user.isActive())
                userDTOS.add(this.modelMapper.map(user, UserDTO.class));
        }

        return ResponseEntity.ok(userDTOS);
    }

    public ResponseEntity<UserDTO> add(UserDTO userDTO) {
        User user = this.modelMapper.map(userDTO, User.class);
        user.setDefaultAttributes();

        User createdUser = this.userRepository.save(user);

        return ResponseEntity.status(CREATED).body(this.modelMapper.map(createdUser, UserDTO.class));
    }

    public ResponseEntity<UserDTO> delete(Integer userId) {
        User user = this.userRepository.findById(userId)
                .orElseThrow(
                        () -> new ApiRequestException(
                                String.format(this.errorMessage, userId),
                                NOT_FOUND
                        )
                );
        user.setUpdatedAt(user.getDate());
        user.setActive(false);

        User updatedUser = this.userRepository.save(user);

        return ResponseEntity.ok(this.modelMapper.map(updatedUser, UserDTO.class));
    }

    public ResponseEntity<UserDTO> update(Integer userId, UserDTO userDTO) {
        User user = this.userRepository.findById(userId)
                .orElseThrow(
                        () -> new ApiRequestException(
                                String.format(this.errorMessage, userId),
                                NOT_FOUND
                        )
                );

        user.setName(userDTO.getName());
        user.setLastName(userDTO.getLastName());
        user.setCellphone(userDTO.getCellphone());
        user.setDocId(userDTO.getDocId());
        user.setUpdatedAt(user.getDate());

        User updatedUser = this.userRepository.save(user);

        return ResponseEntity.ok(this.modelMapper.map(updatedUser, UserDTO.class));
    }

    public ResponseEntity<List<OrchardDTO>> getOrchards(Integer userId) {
        User user = this.userRepository.findById(userId)
                .orElseThrow(
                        () -> new ApiRequestException(
                                String.format(this.errorMessage, userId),
                                NOT_FOUND
                        )
                );

        List<OrchardDTO> orchardDTOS = new ArrayList<>();

        for (Orchard orchard : user.getOrchards()) {
            if (!orchard.isActive()) continue;
            OrchardDTO orchardDTO = this.modelMapper.map(orchard, OrchardDTO.class);
            orchardDTO.setParcelIds(
                    orchard.getParcels()
                            .stream()
                            .map(Parcel::getId)
                            .toList()
            );

            orchardDTOS.add(orchardDTO);
        }

        return ResponseEntity.ok(orchardDTOS);
    }
}
