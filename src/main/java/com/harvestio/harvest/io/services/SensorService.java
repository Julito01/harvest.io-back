package com.harvestio.harvest.io.services;

import com.harvestio.harvest.io.dto.MeasDTO;
import com.harvestio.harvest.io.exception.ApiRequestException;
import com.harvestio.harvest.io.model.Meas;
import com.harvestio.harvest.io.model.Parcel;
import com.harvestio.harvest.io.repository.ParcelRepository;
import com.harvestio.harvest.io.repository.SensorRepository;
import com.harvestio.harvest.io.repository.MeasRepository;
import com.harvestio.harvest.io.model.Sensor;
import com.harvestio.harvest.io.dto.SensorDTO;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.springframework.http.HttpStatus.*;

@Service
public class SensorService {

    private final ParcelRepository parcelRepository;
    private final SensorRepository sensorRepository;
    private final MeasRepository measRepository;
    private final String errorMessage = "El sensor con id [%s] no se encontró.";

    private final ModelMapper modelMapper;

    public SensorService(ParcelRepository parcelRepository,
                         SensorRepository sensorRepository,
                         MeasRepository measRepository,
                         ModelMapper modelMapper) {
        this.parcelRepository = parcelRepository;
        this.sensorRepository = sensorRepository;
        this.measRepository = measRepository;
        this.modelMapper = modelMapper;
    }

    public ResponseEntity<List<SensorDTO>> getAll() {
        List<Sensor> sensors = sensorRepository.findAll();
        List<SensorDTO> sensorDTOS = new ArrayList<>();

        for (Sensor sensor : sensors) {
            if (sensor.isActive())
                sensorDTOS.add(this.modelMapper.map(sensor, SensorDTO.class));
        }

        return ResponseEntity.ok(sensorDTOS);
    }

    public ResponseEntity<SensorDTO> get(Integer sensorId) {
        return ResponseEntity.ok(this.modelMapper.map(this.sensorRepository.findById(sensorId), SensorDTO.class));
    }

    public ResponseEntity<SensorDTO> add(SensorDTO sensorDTO) {
        int parcelId = sensorDTO.getParcelId();
        Parcel parcel = this.parcelRepository.findById(parcelId)
                .orElseThrow(
                        () -> new ApiRequestException(
                                String.format("La parcela con id [%s] no se encontró.", parcelId),
                                NOT_FOUND
                        )
                );

        Sensor sensor = this.modelMapper.map(sensorDTO, Sensor.class);
        sensor.setParcel(parcel);
        sensor.setDefaultAttributes();

        Sensor createdSensor = this.sensorRepository.save(sensor);

        return ResponseEntity.status(CREATED).body(this.modelMapper.map(createdSensor, SensorDTO.class));
    }

    public ResponseEntity<SensorDTO> update(Integer sensorId, SensorDTO sensorDTO) {
        Sensor sensor = this.sensorRepository.findById(sensorId)
                .orElseThrow(
                        () -> new ApiRequestException(
                                String.format(this.errorMessage, sensorId),
                                NOT_FOUND
                        )
                );
        sensor.setSoilMoisture(sensorDTO.getSoilMoisture());
        Sensor updatedSensor = this.sensorRepository.save(sensor);

        return ResponseEntity.ok(this.modelMapper.map(updatedSensor, SensorDTO.class));
    }

    public ResponseEntity<SensorDTO> delete(Integer sensorId) {
        Sensor sensor = sensorRepository.findById(sensorId)
                .orElseThrow(
                        () -> new ApiRequestException(
                                String.format(this.errorMessage, sensorId),
                                NOT_FOUND
                        )
                );
        sensor.setActive(false);

        return ResponseEntity.ok(this.modelMapper.map(sensor, SensorDTO.class));
    }

    public List<Object> getLast30days(Integer parcelId) {
        Instant now = Instant.now();
        Instant nowMinusThirtyDays = now.minus(Duration.ofDays(30));
        Date thirtyDaysAgo = Date.from(nowMinusThirtyDays);

        return measRepository.getAverageSoilLast30Days(parcelId, thirtyDaysAgo);
    }

    public ResponseEntity<List<MeasDTO>> getMeas(Integer sensorId){
        Sensor sensor = this.sensorRepository.findById(sensorId)
                .orElseThrow(
                        () -> new ApiRequestException(
                                String.format(this.errorMessage, sensorId),
                                NOT_FOUND
                        )
                );

        List<MeasDTO> measDTOS = new ArrayList<>();

        for (Meas meas : sensor.getMeasurements()) {
            if (!meas.isActive()) continue;
            MeasDTO measDTO = this.modelMapper.map(meas, MeasDTO.class);
            measDTO.setSensorId(sensor.getId()
            );
            measDTOS.add(measDTO);
        }
        return ResponseEntity.ok(measDTOS);
    }

    public ResponseEntity<SensorDTO> updateMeas(Integer sensorId, MeasDTO measDTO) {
        Sensor sensor = this.sensorRepository.findById(sensorId)
                .orElseThrow(
                        () -> new ApiRequestException(
                                String.format(this.errorMessage, sensorId),
                                NOT_FOUND
                        )
                );
        sensor.setSoilMoisture(measDTO.getRead());
        sensor.setUpdatedAt(sensor.getDate());
        Sensor updatedSensor = this.sensorRepository.save(sensor);

        return ResponseEntity.ok(this.modelMapper.map(updatedSensor, SensorDTO.class));
    }
}
