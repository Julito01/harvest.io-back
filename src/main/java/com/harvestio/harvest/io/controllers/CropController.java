package com.harvestio.harvest.io.controllers;

import com.harvestio.harvest.io.dto.CropDTO;
import com.harvestio.harvest.io.services.CropService;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/crops")
public class CropController {
    public final CropService cropService;

    public CropController (CropService cropService) {
        this.cropService = cropService;
    }

    @GetMapping
    public ResponseEntity<List<CropDTO>> getCrops() {
        return cropService.getAll();
    }

    @GetMapping("{cropId}")
    public ResponseEntity<CropDTO> getCrop(@PathVariable(value = "cropId") final Integer id) {
        return cropService.getCrop(id);
    }

    @PostMapping
    public ResponseEntity<CropDTO> addCrop(@RequestBody final @NotNull CropDTO crop) {
        return cropService.addCrop(crop);
    }
}

