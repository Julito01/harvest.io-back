package com.harvestio.harvest.io.controllers;

import com.harvestio.harvest.io.dto.OrchardDTO;
import com.harvestio.harvest.io.dto.ParcelDTO;
import com.harvestio.harvest.io.services.OrchardService;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/orchards")
public class OrchardController {
    public final OrchardService orchardService;

    public OrchardController(OrchardService orchardService) {
        this.orchardService = orchardService;
    }

    @GetMapping("{orchardId}/parcels")
    public ResponseEntity<List<ParcelDTO>> getParcels(@PathVariable(value = "orchardId") final Integer id) {
        return orchardService.getParcels(id);
    }

    @GetMapping
    public ResponseEntity<List<OrchardDTO>> getOrchards() {
        return orchardService.getAll();
    }

    @GetMapping("{orchardId}")
    public ResponseEntity<OrchardDTO> getOrchard(@PathVariable(value = "orchardId") final Integer id) {
        return orchardService.get(id);
    }

    @PostMapping
    public ResponseEntity<OrchardDTO> postOrchard(@RequestBody final @NotNull OrchardDTO orchard) {
        return orchardService.add(orchard);
    }

    @PutMapping("{orchardId}")
    public ResponseEntity<OrchardDTO> updateOrchard(@PathVariable(value = "orchardId") final Integer id, @RequestBody final @NotNull OrchardDTO orchard) {
        return orchardService.update(id, orchard);
    }

    @DeleteMapping("{orchardId}")
    public ResponseEntity<OrchardDTO> deleteOrchard(@PathVariable(value = "orchardId") final Integer id) {
        return orchardService.delete(id);
    }
}
