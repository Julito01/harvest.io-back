package com.harvestio.harvest.io.controllers;

import com.harvestio.harvest.io.auth.RegisterRequest;
import com.harvestio.harvest.io.model.EmailAlertInfo;
import com.harvestio.harvest.io.services.EmailSenderService;
import jakarta.mail.MessagingException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
@RequestMapping("api/v1/email")
public class EmailController {
    private final EmailSenderService emailService;
    public EmailController(EmailSenderService emailService) {
        this.emailService = emailService;
    }

    @PostMapping("/send")
    public ResponseEntity<HashMap<String, Object>> sendMail(@RequestBody RegisterRequest registerRequest) throws MessagingException {
        return emailService.sendMail(registerRequest);
    }

    @PostMapping ("/alert")
    public ResponseEntity<HashMap<String, Object>> sendAlert (@RequestBody EmailAlertInfo alertRequest) throws MessagingException {
        return emailService.sendAlert(alertRequest);
    }
}
