package com.harvestio.harvest.io.controllers;

import com.harvestio.harvest.io.dto.MeasDTO;
import com.harvestio.harvest.io.services.MeasService;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api/v1/meas")
public class MeasController {
    public final MeasService measService;

    public MeasController (MeasService measService) {
        this.measService = measService;
    }

    @GetMapping
    public ResponseEntity<List<MeasDTO>> getAll() {
        return measService.getAll();
    }

    @PostMapping
    public ResponseEntity<MeasDTO> addMeas(@RequestBody final @NotNull MeasDTO meas) {
        return measService.add(meas);
    }

    @PutMapping("{measId}")
    public ResponseEntity<MeasDTO> updateMeas(@PathVariable(value = "measId") final Integer id, @RequestBody final @NotNull MeasDTO meas) {
        return measService.update(id, meas);
    }

    @DeleteMapping("{measId}")
    public ResponseEntity<Map<String, Object>> deleteMeas(@PathVariable(value = "measId") final Integer id) {
        return measService.delete(id);
    }
}
