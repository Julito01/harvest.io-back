package com.harvestio.harvest.io.controllers;

import com.harvestio.harvest.io.dto.MeasDTO;
import com.harvestio.harvest.io.model.Sensor;
import com.harvestio.harvest.io.dto.SensorDTO;
import com.harvestio.harvest.io.services.SensorService;
import org.jetbrains.annotations.NotNull;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/sensors")
public class SensorController implements ErrorController {
    public final SensorService sensorService;

    public SensorController(SensorService sensorService) {
        this.sensorService = sensorService;
    }

    @GetMapping
    public ResponseEntity<List<SensorDTO>> getAll() {
        return sensorService.getAll();
    }

    @GetMapping("{sensorId}")
    public ResponseEntity<SensorDTO> getById(@PathVariable(value = "sensorId") Integer id) {
        return sensorService.get(id);
    }

    @PostMapping
    public ResponseEntity<SensorDTO> postSensor(@RequestBody final @NotNull SensorDTO sensor) {
        return sensorService.add(sensor);
    }

    @PutMapping("{sensorId}")
    public ResponseEntity<SensorDTO> updateSensor(@PathVariable(value = "sensorId") Integer id, @RequestBody final @NotNull SensorDTO sensor) {
        return sensorService.update(id, sensor);
    }

    @PutMapping("{sensorId}/updateMeas")
    public ResponseEntity<SensorDTO> updateSensorMeas(@PathVariable(value = "sensorId") Integer id, @RequestBody final @NotNull MeasDTO meas) {
        return sensorService.updateMeas(id, meas);
    }

    @DeleteMapping("{sensorId}")
    public ResponseEntity<SensorDTO> deleteSensor(@PathVariable(value = "sensorId") final Integer id, @RequestBody final @NotNull SensorDTO sensor) {
        return sensorService.delete(id);
    }

    @GetMapping("{sensorId}/meas")
    public ResponseEntity<List<MeasDTO>> getMeas(@PathVariable(value = "sensorId") final Integer id) {
        return sensorService.getMeas(id);
    }
}
