package com.harvestio.harvest.io.controllers;

import com.harvestio.harvest.io.dto.ParcelDTO;
import com.harvestio.harvest.io.dto.SensorDTO;
import com.harvestio.harvest.io.services.ParcelService;
import org.jetbrains.annotations.NotNull;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/parcels")
public class ParcelController implements ErrorController {
    public final ParcelService parcelService;

    public ParcelController(ParcelService parcelService) {
        this.parcelService = parcelService;
    }

    @GetMapping
    public List<ParcelDTO> getAll() {
        return parcelService.getAll();
    }

    @GetMapping("{parcelId}")
    public ResponseEntity<ParcelDTO> get(@PathVariable(value = "parcelId") final Integer id) {
        return parcelService.get(id);
    }

    @PostMapping
    public ResponseEntity<ParcelDTO> addParcel(@RequestBody final @NotNull ParcelDTO parcel) {
        return parcelService.add(parcel);
    }

    @PutMapping("{parcelId}")
    public ResponseEntity<ParcelDTO> updateParcel(@PathVariable(value = "parcelId") final Integer id, @RequestBody final @NotNull ParcelDTO parcel) {
        return parcelService.update(id, parcel);
    }

    @DeleteMapping("{parcelId}")
    public ResponseEntity<ParcelDTO> deleteParcel(@PathVariable(value = "parcelId") final Integer id) {
        return parcelService.delete(id);
    }

    @GetMapping("{parcelId}/days")
    public ResponseEntity<List<Object>> getLast30Days(@PathVariable(value = "parcelId") final Integer id){
        return parcelService.getLast30Days(id);
    }

    @GetMapping("{parcelId}/sensors")
    public ResponseEntity<List<SensorDTO>> getSensors(@PathVariable(value = "parcelId") final Integer id){
        return parcelService.getSensors(id);
    }

    @GetMapping("{parcelId}/soil")
    public ResponseEntity<Double> getSoil(@PathVariable(value = "parcelId") final Integer id){
        return parcelService.getSoil(id);
    }
}
