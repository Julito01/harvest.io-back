package com.harvestio.harvest.io.controllers;

import com.harvestio.harvest.io.dto.OrchardDTO;
import com.harvestio.harvest.io.dto.UserDTO;
import com.harvestio.harvest.io.model.User;
import com.harvestio.harvest.io.services.UserService;
import com.sun.net.httpserver.Authenticator.Success;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/users")
public class UserController {

    public final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public ResponseEntity<List<UserDTO>> getUsers() {
        return userService.getAll();
    }

    @GetMapping("{userId}")
    public ResponseEntity<UserDTO> getUser(@PathVariable(value = "userId") final Integer id) {
        return userService.get(id);
    }

    @PostMapping
    public ResponseEntity<UserDTO> postUser(@RequestBody final @NotNull UserDTO user) {
        return userService.add(user);
    }

    @PutMapping("{userId}")
    public ResponseEntity<UserDTO> updateUser(@PathVariable(value = "userId") final Integer id, @RequestBody final @NotNull UserDTO user) {
        return userService.update(id, user);
    }

    @DeleteMapping("{userId}")
    public ResponseEntity<UserDTO> deleteUser(@PathVariable(value = "userId") final Integer id) {
        return userService.delete(id);
    }

    @GetMapping("{userId}/orchards")
    public ResponseEntity<List<OrchardDTO>> getUserOrchards(@PathVariable(value = "userId") final Integer id) {
        return userService.getOrchards(id);
    }
}
