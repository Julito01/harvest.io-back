FROM maven:3.8.6-openjdk-18-slim AS build
WORKDIR /app

COPY ./pom.xml .
RUN mvn dependency:go-offline -B

COPY src ./src
RUN mvn clean package

FROM openjdk:18.0-jdk-slim
COPY --from=build /app/target/*.jar /app/harvest-io.jar
ENTRYPOINT ["java", "-jar", "/app/harvest-io.jar"]